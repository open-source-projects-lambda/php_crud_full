// Ajax for insert data without reloading 
$(document).ready(function(){
    function showdata(){
        output = "";
     $.ajax({
         url:'retrive.php',
         method:'GET',
         dataType:'json',
         success: function(data){
                  if(data){
                   x = data;
               }else{
                   x = "";
               }
               for(i=0;i<x.length;i++){
                   output += "<tr><td>"+x[i].id+"</td><td>"+x[i].name+"</td><td>"+x[i].email+"</td><td>"+x[i].password+"</td><td> <button class='btn btn-warning btn-sm btn-edit'data-sid="+x[i].id+">Edit</button> <button class='btn btn-danger btn-sm btn-del' data-sid="+x[i].id+">Delete</button></td></tr>";
               }
               $("#tbody").html(output);
           },
         }
     );   
    }
 showdata();
    //ajax for insert data
    $("#btnadd").click(function(e){
        e.preventDefault();
       // let stid = $("#stuid").val();
        let nm = $("#nameid").val();
        let em = $("#emailid").val();
        let pw = $("#passwordid").val();
        //console.log(nm);
        //console.log(em);
        //console.log(pw);
        mydata = {name:nm , email:em, password:pw};
        $.ajax({
            url:"insert.php",
            method: "POST",
            data: JSON.stringify(mydata),
            success: function(data){
                //alert("successfully saved")
                msg = "<div class='alert alert-dark mt-3'>"+ data + "</div>";
                $("#msg").html(msg);
                $("#myform")[0].reset();
                showdata();
    
            },
        });
    
    });
    //Ajax data delete
    $('tbody').on("click",".btn-del", function(){
       // console.log("Delete clicked");
        let id = $(this).attr("data-sid");
       // console.log(id);
       mydata = {sid:id};
       mythis = this;
       $.ajax({
           url:"delete.php",
           method:"POST",
           data:JSON.stringify(mydata),
           success: function(data){
               if (data==1){
                msg = "<div class='alert alert-dark mt-3'>"+ "Selected data wiped out" + "</div>"; 
                $(mythis).closest("tr").fadeOut();
               }else if(data === 0){
                    msg = "<div class='alert alert-dark mt-3'>"+ "Something went wrong at the moment please try again "+ "</div>"; 
               }
                $("#msg").html(msg);
              //  showdata();
           
           }
       });
    });
//Ajax for edit data
 $('tbody').on("click",".btn-edit", function(){
        console.log("Edit clicked");
        let id = $(this).attr("data-sid");
        console.log(id);
        mydata = {sid:id};
        $.ajax({
            url: 'edit.php',
            method: 'POST',
            dataType: "json",
            data :JSON.stringify(mydata),
            success:function(data){
                //console.log(data);
                $("#stuid").val(data.id);
                $("#nameid").val(data.name);
                $("#emailid").val(data.email);
                $("#passwordid").val(data.password);
            }
        });
    });
    //Ajax for update data
    $("#btnupdate").click(function(e){
        e.preventDefault();
        let id = $("#stuid").val();
        let nm = $("#nameid").val();
        let em = $("#emailid").val();
        let pw = $("#passwordid").val();
        console.log(id);
        console.log(nm);
        console.log(em);
        console.log(pw);
        mydata = {eid:id, name:nm, email:em, password:pw};
        $.ajax({
            url:"update.php",
            method: "POST",
            data: JSON.stringify(mydata),
            success: function(data){
                console.log(data);
                //alert("successfully saved")
                msg = "<div class='alert alert-dark mt-3'>"+ data + "</div>";
                $("#msg").html(msg);
                $("#myform")[0].reset();
                showdata();
            },
        });
    
    });
});
