
<!Doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <?php
       //include('config.php');
       // include('./retrive.php');
       include('insert.php');
      // include('update.php');
      date_default_timezone_set('Asia/kolkata');
      echo date('<br> l jS \of F Y h:i:s A ');

    ?>
    <div class="container mt-5">
        <h1 class="alert-info text-center mb-5 p-3">
            PHP crud with ajax php jquery
        </h1>
        <div class="row">
            <form action="" method="post" class="col-sm-5" id="myform">
                <h3 class="alert-warning text-center p-2">
                    Add/Update Student
                </h3>
                <div>
                    <input type="text" name="" class="form-control" id="stuid" style="display:none;">
                    <label for="nameid" class="form-label">Name</label>
                    <input type="text" name="name" id="nameid" placeholder="enter full name"  class="form-control" >
                </div>
                <div>
                    <label for="emailid" class="form-label">Email</label>
                    <input type="email" name="email" id="emailid" placeholder="email@example.com" class="form-control">
                </div>
                <div>
                    <label for="passwordid" class="form-label">Password</label>
                    <input type="password" name="password" id="passwordid" class="form-control">
                </div>
                <div class="mt-5">
                    <button type="submit" class="btn btn-primary" id="btnadd"> Save</button>
                    <button type="submit" class="btn btn-danger" id="btnupdate"> Update</button>
                </div>
                <div id="msg"></div>
            </form>
            <div class="col-sm-7 text-center">
                <h3 class="alert-warning p-2"> Show student information</h3>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Password</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tbody"></tbody>
                </table>
            </div>
        </div>
    </div>








    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="js/jqueryajax.js"></script>
  </body>
</html>
